const config = require('lemon/src/config');
const ideas = require('lemon/src/database/ideas');

let initialized;

/*
	part of the task/core idea
	----
	id: id, // self-explanatory, all IDs are truthy
	created: ISO String, // when the ticket was created
	updated: ISO String, // when the ticket was last updated

	props with dedicated values
	----
	name: '', // summary text, title
	description: '', // long form
	tags, // multiple links, categories

	pure links
	----
	parent, // null, undefined; truthy means it has a parent

	links to enums
	----
	type, // epic/task/bug etc
	status, // single link, have we started/finished/etc
	priority, // single link, how important is it (number, higher numbers at the top of the list, unbounded)
*/
config.onInit(() => {
	initialized = new Promise((resolve) => {
		ideas.context('Wumpus Root').then((wumpusRootContext) => {
			Promise.resolve()
				.then(() => ideas.save(wumpusRootContext))
				.then(() => config.save())
				.then(() => resolve());
		});
	});
});

exports.rest = (router) => {
	initialized.then(() => {
		// enums.rest(router, 'priorities', todoContexts.priority);
		// tasks.rest(router, todoContexts);
	});

	return router;
};
