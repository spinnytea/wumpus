// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import 'jest-styled-components'; // eslint-disable-line import/no-extraneous-dependencies

import utils from './utils';
import { resetNextId } from './wumpus/Room';

// bugfix: @testing-library/react (testing-library/dom 7.29.6) ReferenceError: clearImmediate is not defined
window.setImmediate = window.setTimeout;
window.clearImmediate = window.clearTimeout;

beforeEach(() => {
	utils.random.seed('whats the wumpus');
	resetNextId();
});