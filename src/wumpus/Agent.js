import RememberedRoom from './RememberedRoom';
import utils from '../utils';

export default class Agent {
	constructor({
		x = 0, y = 0, r = 0, da = 0, dt = 0,
		cave, inRooms = undefined, rememberRooms = undefined,
		alive = true, gold = false, win = false, hazard = null, moveTo = null,
	}) {
		this.x = x;
		this.y = y;
		this.r = r; // the direction the agent facing
		this.da = da; // derivative of acceleration (velocity)
		this.dt = dt; // derivative of torque (angular velocity)
		this.cave = cave;
		this.inRooms = new Set(inRooms); // REVIEW should inRooms be the actual room, and not just the id?
		this.rememberRooms = new Map(rememberRooms);
		this.alive = alive;
		this.gold = gold;
		this.win = win;
		this.hazard = hazard;
		this.moveTo = moveTo; // a room number to move towards (for the AI)
	}

	clone(updates) {
		return new Agent({ ...this, ...updates });
	}

	sense() {
		if (this.cave.config.game.observable === 'fully') {
			return this.cave;
		}

		if (this.cave.config.game.observable === 'partially' || this.cave.config.game.observable === 'remember') {
			const canSenseWumpus = this.cave.wumpus
				&& Array.from(this.cave.wumpus.inRooms).some((r) => this.inRooms.has(r));

			const remember = {};
			if (this.cave.config.game.observable === 'remember') {
				remember.rooms = Array.from(this.rememberRooms.values());
			}

			return {
				config: this.cave.config,
				agent: this,
				wumpus: (canSenseWumpus ? this.cave.wumpus : null),
				arrow: this.cave.arrow,
				rooms: this.cave.rooms.filter((room) => this.inRooms.has(room.id)),
				remember,
			};
		}

		return null;
	}

	setR(r) {
		// keep r in a reasonable range
		if (r > Math.PI) r -= Math.PI * 2;
		if (r < -Math.PI) r += Math.PI * 2;

		if (this.cave.config.game.grain === 'discrete') {
			// "round" to the nearest cardinal direction
			this.r = Math.floor(r / (Math.PI / 2)) * (Math.PI / 2);
		}
		else if (this.cave.config.game.grain === 'continuous') {
			this.r = r;
		}
	}

	stop() {
		this.da = 0;
		this.dt = 0;
	}

	distance({ x, y }) {
		return Math.sqrt((this.x - x) ** 2 + (this.y - y) ** 2);
	}

	nearbyRooms() {
		const nearbyRooms = new Set();
		this.inRooms.forEach((id) => this.cave.rooms.forEach((room) => { if (room.id === id) room.nearbyRooms.forEach((r) => nearbyRooms.add(r)); }));
		return Array.from(nearbyRooms);
	}

	placeInRoom(room) {
		const { agent, wumpus } = this.cave;

		// update position
		this.x = room.x;
		this.y = room.y;

		// reset movement
		this.da = 0;
		this.dt = 0;

		if (this === agent) {
			this.setR(utils.random.next() * Math.PI * 2);
		}

		if (wumpus && (this === wumpus || this === agent)) {
			// make sure the wumpus is facing away from the agent
			// (this way, if the wumpus is next to the agent, the agent wont immediately lose if it isn't facing the wumpus)
			wumpus.setR(Math.atan2(wumpus.y - agent.y, wumpus.x - agent.x) + Math.PI * 2);
		}

		this.updateInRooms([room]);
	}

	updateInRooms(rooms) {
		if (this === this.cave.wumpus) {
			this.cave.rooms.forEach((room) => { if (this.inRooms.has(room.id)) room.hasWumpus = false; });
			rooms.forEach((room) => { room.hasWumpus = true; });
		}
		if (this === this.cave.agent) {
			if (this.cave.config.game.observable === 'remember') {
				// remember every room that we have been
				this.inRooms.forEach((id) => {
					if (!this.rememberRooms.has(id)) {
						const room = this.cave.rooms.find((r) => id === r.id);
						this.rememberRooms.set(id, new RememberedRoom(room));
					}
				});
			}
		}

		this.inRooms.clear();

		rooms.forEach((room) => {
			this.inRooms.add(room.id);
			if (this.cave.config.game.observable === 'remember') {
				// we do not need to remember the rooms we are in
				this.rememberRooms.delete(room.id);
			}
			// if we got to the room, then reset it
			if (room.id === this.moveTo) {
				this.moveTo = null;
			}
		});

		// if we are trying to move to a room, and it is no longer nearby (we went the wrong way or something)
		// then stop trying to go to that room
		if (this.moveTo !== null) {
			// TODO write a unit test against this
			const canSeeRoom = rooms.some((room) => room.nearbyRooms.some((r) => r.id === this.moveTo.id));
			if (!canSeeRoom) {
				this.moveTo = null;
			}
		}
	}

	// IDEA put "rocks" in the cave instead of blind randomness
	update() {
		const { config } = this.cave;
		let didMove = false;

		if (this.alive) {
			this.hazard = null; // TODO timing === dynamic needs a timeout or step count (only show hazard === 'bat' for a moment)
		}

		if (config.game.chance === 'stochastic'
			&& config.game.grain === 'discrete'
			&& utils.random.next() < config.peas.discrete.chance) {
			if (this === this.cave.arrow) {
				// pick a random nearbyRooms to fire towards instead
				const moveTo = utils.random.pullItem(this.nearbyRooms());
				const angleTo = Math.atan2(moveTo.y - this.y, moveTo.x - this.x);
				this.setR(angleTo);
			}
			else {
				// both the player and the wumpus
				// REVIEW should we do something more interesting than just skipping?
				return { didMove };
			}
		}

		// all the turn regardless
		this.setR(this.r + this.dt);

		if (this.da > 0) {
			if (config.game.chance === 'stochastic'
				&& config.game.grain === 'continuous'
				&& utils.random.next() < config.peas.continuous.chance) {
				if (this === this.cave.arrow) {
					// pick a random nearbyRooms to fire towards instead
					const moveTo = utils.random.pullItem(this.nearbyRooms());
					const angleTo = Math.atan2(moveTo.y - this.y, moveTo.x - this.x);
					this.setR(angleTo);
				}
				else {
					// both the player and the wumpus
					this.setR(this.r + (utils.random.next() - 0.5) * (Math.PI / 2)); // random angle
					this.da -= utils.random.next() * 0.5 * this.da; // random speed decrease
				}
			}

			// calculate where we will be if we move forward
			// if we are no longer in any rooms, then we cannot move
			const peek = {
				x: this.x + Math.cos(this.r) * this.da,
				y: this.y + Math.sin(this.r) * this.da,
			};

			const peekRooms = this.cave.rooms.filter((room) => room.distance(peek) < config.room.radius);
			let canMove = false;
			if (config.game.grain === 'discrete') {
				canMove = peekRooms.length > 0;
			}
			else if (config.game.grain === 'continuous') {
				// if I am moving to a room I am already in, done
				canMove = peekRooms.some((room) => this.inRooms.has(room.id));
				if (!canMove && this === this.cave.arrow) {
					// if the rooms I am going to has a door to the room I am in
					// (if the step was so large we skipped the overlap entirely)
					// this is necessary for the arrow, because it moves so fast
					// and "crooked arrows" have the "ability to change direction while in flight"
					// XXX unit test
					this.cave.rooms.forEach((room) => {
						if (this.inRooms.has(room.id)) {
							canMove = canMove || peekRooms.some((r) => room.nearbyRooms.includes(r));
						}
					});
				}
			}

			// if we can move forward, then update our location
			if (canMove) {
				this.updateInRooms(peekRooms);

				if (config.game.grain === 'discrete') {
					// set exactly to avoid floating point drift
					this.x = peekRooms[0].x;
					this.y = peekRooms[0].y;
				}
				else if (config.game.grain === 'continuous') {
					this.x = peek.x;
					this.y = peek.y;
				}

				didMove = true;
			}
		}

		return { didMove };
	}
}
