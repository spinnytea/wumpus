import { useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { gameActions } from '../redux';

export default function Gameplay() {
	const { cave } = useSelector((state) => state.game);
	const timing = cave.config.game.timing; // eslint-disable-line prefer-destructuring
	const updateDelay = 1000 / cave.config.peas[cave.config.game.grain].updatesPerSecond;

	const handleKeyDown = useCallback((event) => {
		gameActions.handleKeyDown(event, cave);
	}, [cave]);

	useEffect(() => {
		window.addEventListener('keydown', handleKeyDown);
		return () => {
			window.removeEventListener('keydown', handleKeyDown);
		};
	}, [handleKeyDown]);

	useEffect(() => {
		gameActions.pauseGame(false);
		return () => {
			gameActions.pauseGame(true);
		};
	}, []);

	useEffect(() => {
		let timeoutId;
		function step() {
			gameActions.stepGame();
			timeoutId = setTimeout(step, updateDelay);
		}

		if (timing === 'dynamic') {
			timeoutId = setTimeout(step, updateDelay);
		}

		return () => {
			clearTimeout(timeoutId);
		};
	}, [timing, updateDelay]);

	if (!cave) {
		return null;
	}

	return null;
}
