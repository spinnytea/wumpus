function* NEXT_ID_GENERATOR() {
	let id = 0;
	while (true) {
		yield id;
		id += 1;
	}
}
let NEXT_ID = NEXT_ID_GENERATOR();
export function resetNextId() { NEXT_ID = NEXT_ID_GENERATOR(); }

export default class Room {
	constructor({ x, y, id = null, nearbyRooms = [], hasExit = false, hasGold = false, hasPit = false, hasBat = false, hasWumpus = false }) {
		this.x = x;
		this.y = y;
		this.id = (id !== null ? id : NEXT_ID.next().value);
		this.nearbyRooms = Array.from(nearbyRooms);

		this.hasExit = hasExit;
		this.hasGold = hasGold;
		this.hasPit = hasPit;
		this.hasBat = hasBat;
		this.hasWumpus = hasWumpus;
	}

	distance({ x, y }) {
		return Math.sqrt((this.x - x) ** 2 + (this.y - y) ** 2);
	}

	sense() {
		// REVIEW refactor room.nearbyRooms to a Set of IDs (instead of an array)
		//  - this is the only place where we need the actual room object, other places are the ID or just the count
		//  - but this requires we change cave.rooms to a Map<id -> Room>
		//  - which is fine, because all the cave.room iterators are checking by room id _anyway_ ... except for render
		return this.nearbyRooms.reduce((ret, room) => {
			ret.hasExit = ret.hasExit || room.hasExit; // ¿taste? the exit (exit)
			ret.hasGold = ret.hasGold || room.hasGold; // see a glint (gold)
			ret.hasPit = ret.hasPit || room.hasPit; // feel a draft (pit)
			ret.hasBat = ret.hasBat || room.hasBat; // hear a bat (bat)
			ret.hasWumpus = ret.hasWumpus || room.hasWumpus; // smell a Wumpus (wumpus)
			return ret;
		}, {
			hasExit: false,
			hasGold: false,
			hasPit: false,
			hasBat: false,
			hasWumpus: false,
		});
	}

	doors() {
		// XXX memoize, since nearbyRooms doesn't change
		return this.nearbyRooms.map((room) => ({
			x: (this.x + room.x) / 2 - this.x,
			y: (this.y + room.y) / 2 - this.y,
		}));
	}
}
