import Room from './Room';

export default class RememberedRoom extends Room {
	constructor({ ...rest }) {
		super(rest);

		this.sensed = super.sense();
	}

	sense() {
		return this.sensed;
	}
}
