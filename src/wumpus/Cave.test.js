import { isFunction } from 'lodash';

import { getConfig } from '../Test';
import { roomOffset } from './peas/grain';
import Agent from './Agent';
import Cave from './Cave';
import Room from './Room';

describe('Cave', () => {
	function prepCaveForSnapshot(cave) {
		const clone = new Cave({
			...cave,
			agent: new Agent(cave.agent),
			rooms: cave.rooms.map((room) => new Room(room)),
		});
		clone.agent.rememberRooms = `Map of [${Array.from(cave.agent.rememberRooms.keys())}]`;
		clone.rooms.forEach((room) => {
			room.nearbyRooms = `Array of [${room.nearbyRooms.map((r) => r.id)}]`;
		});
		return clone;
	}

	test('ddfsksp', () => {
		const config = getConfig({});
		const cave = Cave.generate(config);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot();
	});

	test.todo('Sdfsksp');

	test('dCfsksp', () => {
		const config = getConfig({ grain: 'continuous' });
		const cave = Cave.generate(config);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot();
	});

	test('ddRsksp', () => {
		const config = getConfig({ observable: 'remember' });
		const cave = Cave.generate(config);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot();

		// move forward to prove
		const next = cave.update({ forward: true });
		expect(prepCaveForSnapshot(next)).toMatchSnapshot();
	});

	test('ddPsksp', () => {
		// this doesn't change anything about the cave itself
		const config = getConfig({ observable: 'partially' });
		const cave = Cave.generate(config);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot();
	});

	test.todo('ddfSksp');

	test.todo('ddfsKsp');

	test('ddfskMp', () => {
		const config = getConfig({ agents: 'multiple' });
		const cave = Cave.generate(config);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot();

		const wumpusRoom = 2;
		const newWumpusRoom = 3;
		expect(cave.wumpus.inRooms).toEqual(new Set([wumpusRoom]));
		cave.wumpus.placeInRoom(cave.rooms[newWumpusRoom]);
		expect(prepCaveForSnapshot(cave)).toMatchSnapshot('updateInRooms');
	});

	test.todo('ddfsksL');

	// TODO render WumpusSighting?
	describe('update', () => {
		let cave;
		function createCave({ config = {}, rooms = [[0, 0, {}]] }) {
			cave = new Cave({ config: getConfig(config) });

			rooms.forEach(([x, y, hazards]) => {
				if (isFunction(x)) x = x(cave.config);
				if (isFunction(y)) y = y(cave.config);
				const room = new Room({ x, y });
				Object.assign(room, hazards);
				cave.rooms.push(room);
			});

			cave.agent.placeInRoom(cave.rooms[0]);
			cave.agent.setR(0);
		}

		function checkCave() {
			return new Cave({ ...cave, config: null });
		}

		function checkAgent(which = 'agent') {
			const agent = cave[which];
			if (!agent) return agent;
			return new Agent({ ...agent, cave: null });
		}

		describe('keys', () => {
			describe('movement', () => {
				describe('discrete', () => {
					test('left', () => {
						createCave({ config: { grain: 'discrete' } });

						cave = cave.update({ left: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ left: true }).update({ left: true }).update({ left: true });
						expect(checkAgent()).toMatchSnapshot();
					});

					test('right', () => {
						createCave({ config: { grain: 'discrete' } });

						cave = cave.update({ right: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ right: true }).update({ right: true }).update({ right: true });
						expect(checkAgent()).toMatchSnapshot();
					});

					test('forward', () => {
						createCave({ config: { grain: 'discrete' }, rooms: [[0, 0, {}], [roomOffset, 0, {}]] });

						cave = cave.update({ forward: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ forward: true });
						expect(checkAgent()).toMatchSnapshot();
					});

					test('backward', () => {
						createCave({ config: { grain: 'discrete' } });

						const next = cave.update({ backward: true });
						expect(cave).toEqual(next); // nothing changes
					});
				});

				describe('continuous', () => {
					test('left', () => {
						createCave({ config: { grain: 'continuous' } });

						cave = cave.update({ left: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ left: true }).update({ left: true }).update({ left: true });
						expect(checkAgent()).toMatchSnapshot();
					});

					test('right', () => {
						createCave({ config: { grain: 'continuous' } });

						cave = cave.update({ right: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ right: true }).update({ right: true }).update({ right: true });
						expect(checkAgent()).toMatchSnapshot();
					});

					// catch agent in both rooms
					// forward until stops
					test('forward', () => {
						createCave({ config: { grain: 'continuous' }, rooms: [[0, 0, {}], [roomOffset, 0, {}]] });

						cave = cave.update({ forward: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ forward: true }).update({ forward: true }).update({ forward: true });
						expect(checkAgent()).toMatchSnapshot();

						// rooms have radius of 24, spaced 36 apart = overlaps 18 - 30 = 12
						// current speed of… 4
						expect(cave.agent.da).toBe(4);
						expect(cave.agent.x).toBe(10);
						expect(cave.agent.inRooms).toEqual(new Set([0]));
						cave = cave.update({ noop: true });
						expect(cave.agent.x).toBe(14);
						expect(cave.agent.inRooms).toEqual(new Set([0, 1]));
						cave = cave.update({ noop: true }).update({ noop: true });
						expect(cave.agent.x).toBe(22);
						expect(cave.agent.inRooms).toEqual(new Set([0, 1]));
						cave = cave.update({ noop: true });
						expect(cave.agent.da).toBe(4);
						expect(cave.agent.x).toBe(26);
						expect(cave.agent.inRooms).toEqual(new Set([1]));
						expect(checkAgent()).toMatchSnapshot();

						cave = cave.update({ noop: true }).update({ noop: true }).update({ noop: true }).update({ noop: true });
						cave = cave.update({ noop: true }).update({ noop: true }).update({ noop: true });
						expect(cave.agent.x).toBe(54);
						cave = cave.update({ noop: true }).update({ noop: true }).update({ noop: true }).update({ noop: true });
						expect(cave.agent.x).toBe(58);
						expect(checkAgent()).toMatchSnapshot();
					});

					test('backward', () => {
						createCave({ config: { grain: 'continuous' } });

						const next = cave.update({ backward: true });
						expect(cave).toEqual(next); // nothing changes

						cave = cave.update({ forward: true });
						expect(checkAgent()).toMatchSnapshot();
						cave = cave.update({ backward: true });
						expect(checkAgent()).toMatchSnapshot();
						expect(cave.agent.da).toBe(0);
					});
				});
			});

			test.todo('grab');

			test.todo('exit');

			describe('fire', () => {
				describe('discrete', () => {
					test('movement', () => {
						createCave({ config: { grain: 'discrete' }, rooms: [[0, 0, {}], [roomOffset, 0, {}]] });

						expect(cave.arrow).toBe(null);

						cave = cave.update({ fire: true });
						expect(checkCave()).toMatchSnapshot();
						expect(cave.arrow.alive).toBe(true);
						expect(cave.arrow.inRooms).not.toEqual(cave.agent.inRooms);

						cave = cave.update({ noop: true });
						expect(cave.arrow).toBe(null);
					});

					test('immediate death', () => {
						// face a wall and fire
						createCave({ config: { grain: 'discrete' }, rooms: [[0, 0, {}]] });

						expect(cave.arrow).toBe(null);

						cave = cave.update({ fire: true });
						expect(cave.arrow.x).toBe(0);
						expect(cave.arrow.alive).toBe(false);
						expect(cave.arrow.inRooms).toEqual(new Set([0]));

						cave = cave.update({ noop: true });
						expect(cave.arrow).toBe(null);
					});
				});

				describe('continuous', () => {
					test('movement', () => {
						createCave({ config: { grain: 'continuous' }, rooms: [[0, 0, {}], [roomOffset, 0, {}]] });

						expect(cave.arrow).toBe(null);

						cave = cave.update({ fire: true });
						expect(checkCave()).toMatchSnapshot();
						expect(cave.arrow.alive).toBe(true);
						expect(cave.arrow.inRooms).toEqual(new Set([0, 1]));

						cave = cave.update({ noop: true });
						expect(cave.arrow.alive).toBe(true);
						expect(cave.arrow.inRooms).toEqual(new Set([1]));

						cave = cave.update({ noop: true });
						expect(checkAgent('arrow')).toMatchSnapshot();
						expect(cave.arrow.alive).toBe(true);
						expect(cave.arrow.inRooms).toEqual(new Set([1]));

						cave = cave.update({ noop: true });
						expect(cave.arrow).toBe(null);
					});

					test('immediate death', () => {
						// face a wall and fire
						createCave({ config: { grain: 'continuous' }, rooms: [[0, 0, {}]] });
						cave.agent.x = 20;

						expect(cave.arrow).toBe(null);

						cave = cave.update({ fire: true });
						expect(cave.arrow.x).toBe(20);
						expect(cave.arrow.alive).toBe(false);
						expect(cave.arrow.inRooms).toEqual(new Set([0]));

						cave = cave.update({ noop: true });
						expect(cave.arrow).toBe(null);
					});
				});
			});

			test.todo('noop');
		});

		describe('hazards', () => {
			test('bat', () => {
				createCave({ rooms: [[0, 0, { hasExit: true }], [roomOffset, 0, { hasBat: true }]] });

				expect(checkCave()).toMatchSnapshot();
				cave = cave.update({ forward: true });
				expect(checkCave()).toMatchSnapshot();
			});

			test('pit', () => {
				createCave({ rooms: [[0, 0, {}], [roomOffset, 0, { hasPit: true }]] });

				expect(checkCave()).toMatchSnapshot();
				cave = cave.update({ forward: true });
				expect(checkCave()).toMatchSnapshot();
			});

			test('bat + pit', () => {
				createCave({ rooms: [[0, 0, { hasExit: true }], [roomOffset, 0, { hasBat: true, hasPit: true }]] });

				expect(checkCave()).toMatchSnapshot();
				cave = cave.update({ forward: true }); // hit bat
				expect(checkCave()).toMatchSnapshot();
				cave = cave.update({ left: true }); // face door, hazard falls off
				expect(checkCave()).toMatchSnapshot();
				cave = cave.update({ forward: true }); // hit pit, dies
				expect(checkCave()).toMatchSnapshot();
			});

			test.todo('wumpus');
		});
	});
});