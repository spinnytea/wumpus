import { move, roomOffset } from './grain';
import Agent from '../Agent';
import utils from '../../utils';

/**
	this restates timing (static vs dynamic)
	otherwise, dynamic only applies to continuous
	... unless we redo the way movement works in discrete (like, multiple)
*/
export function wanderAi(wumpus) {
	if (!wumpus?.alive) return wumpus;

	if (wumpus.moveTo === null) {
		// if we don't already have a room to move towards, then pick a random room to move to
		const moveTo = utils.random.pullItem(wumpus.nearbyRooms());
		return new Agent({ ...wumpus, moveTo });
	}

	const config = wumpus.cave.config; // eslint-disable-line prefer-destructuring
	const to = wumpus.moveTo;
	let delta = wumpus.r - Math.atan2(to.y - wumpus.y, to.x - wumpus.x);
	if (Math.abs(delta + Math.PI * 2) < Math.abs(delta)) delta += Math.PI * 2;
	if (Math.abs(delta - Math.PI * 2) < Math.abs(delta)) delta -= Math.PI * 2;
	const threshold = Math.PI / 4;
	const moveGrain = move[config.game.grain];

	if (delta > threshold) {
		if (wumpus.dt >= -config.agent.torque) {
			// the agent can turn left at 2x speed
			return moveGrain.left(config, moveGrain.backward(config, wumpus));
		}
	}
	else if (delta < -threshold) {
		if (wumpus.dt <= config.agent.torque) {
			// the agent can turn right at 2x speed
			return moveGrain.right(config, moveGrain.backward(config, wumpus));
		}
	}

	/*
	// XXX keep the agent nudged in the correct direction
	//  - this would be nice but the agent tends to get stuck on walls and corners
	//  - when the agent is "near a door" but not facing it
	//  - it might even out and head straight, but the door is too small (just to the side of where it is)
	//  - and then it gets stuck there forever
	// ----
	//  - the "keep turning" method prevents the wumpus from getting stuck like this
	//  - it looks weird, but it's effective
	// ----
	//  - if we are going to "even out" we need to head for the DOOR to the room first, and once we get there, then we can head to the ROOM
	else {
		if (wumpus.dt >= config.agent.torque) {
			return moveGrain.left(config, wumpus);
		}
		if (wumpus.dt <= -config.agent.torque) {
			return moveGrain.right(config, wumpus);
		}
	}
	*/

	return moveGrain.forward(config, wumpus, config.agent.wumpus_da_limit);
}

export function fireInit(arrow) {
	const config = arrow.cave.config; // eslint-disable-line prefer-destructuring

	if (config.game.grain === 'discrete') {
		// const moveGrain = move[config.game.grain];
		// return moveGrain.forward(config, arrow);
		return arrow.clone({ da: roomOffset(config) });
	}
	if (config.game.grain === 'continuous') {
		return arrow.clone({ da: roomOffset(config) / 2 });
	}

	return null;
}
