import Room from '../Room';
import utils from '../../utils';

export function roomOffset(config) { return config.room.radius * config.room.spacing; }

export const roomFrontier = {
	discrete: (cave, room) => {
		// using a square grid because it makes it visibily different from the continuous one
		const list = [
			new Room({ x: room.x - roomOffset(cave.config), y: room.y }),
			new Room({ x: room.x + roomOffset(cave.config), y: room.y }),
			new Room({ x: room.x, y: room.y - roomOffset(cave.config) }),
			new Room({ x: room.x, y: room.y + roomOffset(cave.config) }),
		];
		while (list.length > cave.config.peas.discrete.branches) {
			utils.random.pullItem(list);
		}
		return list;
	},
	continuous: (cave, room) => {
		const ret = [];
		while (ret.length < cave.config.peas.continuous.branches) {
			const r = utils.random.next() * 2 * Math.PI;
			ret.push(new Room({
				x: Math.cos(r) * roomOffset(cave.config) + room.x,
				y: Math.sin(r) * roomOffset(cave.config) + room.y,
			}));
		}
		return ret;
	},
};

export const move = {
	discrete: {
		left: (config, agent) => agent.clone({ dt: -Math.PI / 2 }),
		right: (config, agent) => agent.clone({ dt: Math.PI / 2 }),
		forward: (config, agent) => agent.clone({ da: roomOffset(config) }),
		backward: (config, agent) => agent,
	},
	continuous: {
		left: (config, agent) => agent.clone({ dt: Math.max(agent.dt - config.agent.torque, -config.agent.dt_limit) }),
		right: (config, agent) => agent.clone({ dt: Math.min(agent.dt + config.agent.torque, config.agent.dt_limit) }),
		forward: (config, agent, da_limit = config.agent.da_limit) => agent.clone({ da: Math.min(agent.da + config.agent.acceleration, da_limit) }), // eslint-disable-line camelcase, max-len
		backward: (config, agent) => agent.clone({ da: Math.max(agent.da - config.agent.acceleration, 0) }),
	},
};

export const grainCleanup = {
	discrete: (cave) => {
		// reset the player's movement
		cave.agent.stop();

		// reset the wumpus movement
		if (cave.wumpus) {
			cave.wumpus.stop();
		}
	},
	continuous: () => {},
};
