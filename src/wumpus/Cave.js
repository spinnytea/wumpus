import { cloneDeep } from 'lodash';

import { fireInit, wanderAi } from './peas/agents';
import { roomFrontier, move, grainCleanup } from './peas/grain';
import Agent from './Agent';
import Room from './Room';
import utils from '../utils';

export default class Cave {
	constructor({ config, agent, wumpus, arrow, rooms = [] }) {
		this.config = config;
		this.agent = new Agent({ ...agent, cave: this });
		this.wumpus = (wumpus ? new Agent({ ...wumpus, cave: this }) : null);
		this.arrow = (arrow ? new Agent({ ...arrow, cave: this }) : null);
		this.rooms = rooms;
	}

	static generate(config) {
		config = cloneDeep(config);

		const cave = new Cave({ config, wumpus: (config.game.agents === 'multiple' ? {} : null) });

		const goldRoom = Math.floor(cave.config.room.count / 2);
		// the wumpus is placed "between" the exit and the gold (not all caves will be made the same)
		const wumpusRoom = (goldRoom - Math.floor(utils.random.next() * utils.random.next() * goldRoom)) || 1;

		let room = new Room({ x: 0, y: 0 });
		room.hasExit = true;
		cave.rooms.push(room);
		const frontier = []; // list of rooms to pick from
		Array.prototype.push.apply(frontier, roomFrontier[config.game.grain](cave, room));

		while (frontier.length > 0 && cave.rooms.length < cave.config.room.count) {
			room = utils.random.pullItem(frontier);

			const roomExists = cave.rooms.some((r) => room.distance(r) < config.room.radius);
			room.nearbyRooms = cave.rooms.filter((r) => room.distance(r) < config.room.radius * 2);
			const skipRoom = roomExists
				|| room.nearbyRooms.length > 3
				|| room.nearbyRooms.some((r) => r.nearbyRooms.length > 2);

			if (!skipRoom) {
				// add hazards to the room
				if (cave.rooms.length === goldRoom) {
					room.hasGold = true;
				}
				if (cave.rooms.length > goldRoom) {
					// only add pits and bats after the gold
					// this ensures that we can make it to the gold
					if (utils.random.next() < config.room.pitProbability) {
						room.hasPit = true;
					}
					if (utils.random.next() < config.room.batProbability) {
						room.hasBat = true;
					}
				}

				cave.addRoom(room);
				Array.prototype.push.apply(frontier, roomFrontier[config.game.grain](cave, room));
			}
		}

		cave.rooms.forEach((r, idx) => { r.id = idx; });
		cave.agent.placeInRoom(cave.rooms.find((r) => r.hasExit));
		if (cave.wumpus) {
			cave.wumpus.placeInRoom(cave.rooms[wumpusRoom]);
		}

		return cave;
	}

	addRoom(room) {
		this.rooms.push(room);
		room.nearbyRooms.forEach((r) => r.nearbyRooms.push(room));
	}

	/*
	// XXX this is breaking `agent.sense()`, we are still seeing gold when we go back to the room
	updateRoom(room, updates) {
		const idx = this.rooms.findIndex((r) => r.id === room.id);
		this.rooms.splice(idx, 1, new Room({ ...room, ...updates }));
	}
	*/

	update(keys) {
		const next = new Cave(this); // REVIEW we should clone rooms
		const { config } = next;

		/* handle keys */
		// REVIEW game.keydown

		if (next.agent.alive) {
			const moveGrain = move[config.game.grain];
			if (keys.left) next.agent = moveGrain.left(config, next.agent);
			if (keys.right) next.agent = moveGrain.right(config, next.agent);
			if (keys.forward) next.agent = moveGrain.forward(config, next.agent);
			if (keys.backward) next.agent = moveGrain.backward(config, next.agent);

			if (keys.grab && !next.agent.gold) {
				const goldRoom = next.rooms.find((room) => (room.hasGold && next.agent.inRooms.has(room.id)));
				if (goldRoom) {
					next.agent.gold = true;
					goldRoom.hasGold = false;
				}
			}

			if (keys.exit && next.agent.gold) {
				const exitRoom = next.rooms.find((room) => (room.hasExit && next.agent.inRooms.has(room.id)));
				if (exitRoom) {
					next.agent.win = true;
				}
			}

			if (keys.fire && !next.arrow) {
				next.arrow = new Agent({
					x: next.agent.x,
					y: next.agent.y,
					r: next.agent.r,
					inRooms: next.agent.inRooms,
					cave: next.agent.cave,
				});
				next.arrow = fireInit(next.arrow);
			}
		}

		/* step simulation */
		// REVIEW game.update

		next.agent = next.agent.clone();
		next.agent.update();
		if (next.wumpus) {
			next.wumpus = wanderAi(next.wumpus);
			next.wumpus.update();
		}
		if (next.arrow) {
			next.arrow = next.arrow.clone();
			const { didMove } = next.arrow.update();

			// if it doesn't move, it dies
			if (!didMove) {
				next.arrow.alive = false;
			}

			if (next.arrow.alive) {
				// if arrow hits the agent, the agent dies
				if (next.arrow.distance(next.agent) < config.agent.radius_arrow + config.agent.radius) {
					// XXX unit test
					next.agent.alive = false;
					next.agent.hazard = 'arrow';
					next.agent.stop();
					next.arrow.alive = false;
				}
				// if arrow hits the wumpus, the wumpus dies
				if (next.wumpus) {
					if (next.arrow.distance(next.wumpus) < config.agent.radius_arrow + config.agent.radius) {
						// XXX unit test
						next.wumpus.alive = false;
						next.wumpus.hazard = 'arrow';
						next.wumpus.stop();
						next.arrow.alive = false;
					}
				}
			}

			// let it live for at least one paint
			if (this.arrow && !next.arrow.alive) {
				next.arrow = null;
			}
		}

		/* check hazards */
		const inBat = next.rooms.find((room) => (room.hasBat && next.agent.inRooms.has(room.id)));
		if (inBat) {
			// REVIEW if an agent encounters a bat, should we update the agent's remembered rooms?
			//  - we only have to do this one room `agent.clearHazard(inBat, 'bat')`
			//  - or I guess we could call `agent.updateInRooms(agent.inRooms)`
			//  - is this part of `apriori === 'known'`
			inBat.hasBat = false;
			next.agent.hazard = 'bat';
			if (config.game.chance === 'deterministic') {
				const exitRoom = next.rooms.find((room) => room.hasExit);
				next.agent.placeInRoom(exitRoom);
			}
			else if (config.game.chance === 'stochastic') {
				const randomRoom = utils.random.pullItem(next.rooms);
				next.rooms.push(randomRoom);
				next.agent.placeInRoom(randomRoom);
			}
		}

		const inPit = next.rooms.find((room) => (room.hasPit && next.agent.inRooms.has(room.id)));
		if (inPit) {
			next.agent.alive = false;
			next.agent.hazard = 'pit';
		}

		const inWumpus = (next.wumpus?.alive && next.wumpus.distance(next.agent) < config.agent.radius * 2);
		if (inWumpus) {
			next.agent.alive = false;
			next.agent.hazard = 'wumpus';
		}

		/* cleanup */

		grainCleanup[config.game.grain](next);

		return next;
	}
}
