import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';

import PageFooter from './components/PageFooter';
import ConfigPage from './routes/ConfigPage';
import HuntTheWumpusPage from './routes/HuntTheWumpusPage';
import SplashPage from './routes/SplashPage';

const StyledTypography = styled(Typography)`
	color: ${(props) => props.theme.palette.text.primary};
	background-color: ${(props) => props.theme.palette.background.default};

	// weird complexity, but adds polish - ensures the footer is at the bottom
	// if we get rid of this, we can remove the section from ConnectedRoot
	min-height: 100vh;
	display: flex;
	flex-direction: column;
	section {
		flex: 1;
	}
`;

export default function ConnectedRoot() {
	return (
		<StyledTypography variant="body1" component="div">
			<section>
				<Routes>
					<Route path="/wumpus" element={<HuntTheWumpusPage key="HuntTheWumpusPage" />} />
					<Route path="/config" element={<ConfigPage key="ConfigPage" />} />
					<Route path="/splash" element={<SplashPage key="SplashPage" />} />
					<Route
						path="*"
						element={<Navigate to="/wumpus" replace />}
					/>
				</Routes>
			</section>

			<PageFooter />
		</StyledTypography>
	);
}
