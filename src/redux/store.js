import { combineReducers, createStore } from 'redux';
import configReducer from './reducers/configReducer';
import gameReducer from './reducers/gameReducer';

const store = createStore(combineReducers({
	config: configReducer,
	game: gameReducer,
}));

export default store;
