import * as configActions from './actions/configActions';
import * as gameActions from './actions/gameActions';
import store from './store';

export {
	configActions,
	gameActions,
	store,
};
