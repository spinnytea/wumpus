export default {
	config: {
		game: {
			chance: 'deterministic', // deterministic, stochastic
			grain: 'discrete', // REVIEW discrete, // REVIEW continuous
			observable: 'remember', // fully, remember, partially
			timing: 'static', // static, // REVIEW dynamic
			apriori: 'known', // REVIEW known, // REVIEW unknown
			agents: 'single', // single, // REVIEW multiple
			player: 'person', // person, lemon
		},

		peas: {
			discrete: {
				branches: 3, // 1 through 4
				chance: 0.1, // how likely is it for the actions to mess up?
				updatesPerSecond: 3, // how long do we wait between updates (only applies to dynamic)
			},
			continuous: {
				branches: 6, // 1 through Infinity, but only 6 will actually fit
				chance: 0.05, // how likely is it for the actions to mess up?
				updatesPerSecond: 10, // how long do we wait between updates (only applies to dynamic)
			},
		},

		// more game details

		// how big are the agents
		agent: {
			radius: 12,
			radius_arrow: 6,

			// XXX config based on refresh rate; something like "turns per second"
			// XXX apply force based on update interval
			acceleration: 1,
			da_limit: 12,
			torque: Math.PI / 40,
			dt_limit: Math.PI / 8,
			wumpus_da_limit: 2,
		},

		room: {
			count: 20, // how many rooms are there? min of 5
			radius: 30, // how big the room is
			spacing: 1.8, // how far apart are the rooms, between 1 and 2 (barely touching) - 0 is identical, 1 is how we treat them as "the same" when we gen
			pitProbability: 0.25,
			batProbability: 0.25,
		},
	},

	game: {
		active: false, // is there a game running - synonymous with `came !== null`
		cave: null,
		paused: true,
		keys: {
			left: false,
			right: false,
			forward: false,
			backward: false,
			grab: false,
			exit: false,
			fire: false,
			noop: false,
		},
	},
};
