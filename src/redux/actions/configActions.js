import store from '../store';

export function startGame() {
	// we need config in the reducer
	store.dispatch({ type: 'SET_ACTIVE', active: true, config: store.getState().config });
}

export function endGame() { // REVIEW this is never called, dup of `gameActions.endGame()`
	store.dispatch({ type: 'SET_ACTIVE', active: false });
}

export function updateConfig(updates) {
	store.dispatch({ type: 'UPDATE_CONFIG', updates });
}
