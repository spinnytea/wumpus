import store from '../store';

export function startGame() {
	// we need config in the reducer
	store.dispatch({ type: 'SET_ACTIVE', active: true, config: store.getState().config });
}

export function stepGame(keysIn) {
	const { cave, keys } = store.getState().game;
	// keys are single use; move the game state forward using the keys, then reset them
	store.dispatch({ type: 'UPDATE_GAME', cave: cave.update(keysIn || keys), keys: {} });
}

export function endGame() { // REVIEW this is never called, dup of `configActions.endGame()`
	store.dispatch({ type: 'SET_ACTIVE', active: false });
}

export function pauseGame(bool) {
	store.dispatch({ type: 'UPDATE_GAME', paused: !!bool });
}

export function handleKeyDown(event, cave) {
	const { config } = cave;
	const keys = { ...store.getState().game.keys };

	let used = true;
	switch (event.keyCode) {
	case 37: keys.left = true; break;
	case 38: keys.forward = true; break;
	case 39: keys.right = true; break;
	case 40:
		if (config.game.grain === 'continuous') keys.backward = true;
		else used = false;
		break;
	case 32:
		if (config.game.timing === 'static') keys.noop = true;
		else used = false;
		break;

	case 71: keys.grab = true; break;
	case 69: keys.exit = true; break;
	case 70:
		if (config.game.agents === 'multiple') keys.fire = true;
		else used = false;
		break;

	default:
		used = false;
	}

	if (used) {
		event.preventDefault();
		if (config.game.timing === 'static') {
			stepGame(keys);
		}
		else if (config.game.timing === 'dynamic') {
			store.dispatch({ type: 'UPDATE_GAME', keys });
		}
	}
}
