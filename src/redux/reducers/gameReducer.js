import initialState from '../initialState';
import Cave from '../../wumpus/Cave';

export default function enumReducer(state = initialState.game, { type, ...action }) {
	switch (type) {
	case 'SET_ACTIVE': {
		let cave = null;

		if (action.active) {
			cave = Cave.generate(action.config);
		}
		else {
			// XXX does endGame need to do anything else?
			// REVIEW this is never called
		}

		return {
			...state,
			active: action.active,
			cave,
		};
	}

	case 'UPDATE_GAME':
		return {
			...state,
			...action,
		};

	default:
		return state;
	}
}
