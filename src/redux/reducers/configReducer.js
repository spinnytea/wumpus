import initialState from '../initialState';

export default function enumReducer(state = initialState.config, action) {
	switch (action.type) {
	case 'UPDATE_CONFIG': {
		const newState = {
			...state,
			...action.updates,
		};
		newState.room.count = Math.min(Math.max(5, newState.room.count), 1000);
		return newState;
	}

	default:
		return state;
	}
}
