import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { StylesProvider, ThemeProvider as MuiThemeProvider } from '@material-ui/core';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';

import './index.css';
import ConnectedRoot from './ConnectedRoot';
import { store } from './redux';
import theme from './theme';

// globally color the body to match the style
document.body.style['background-color'] = theme.palette.background.default;

ReactDOM.render(
	// XXX material-ui: findDOMNode is deprecated in StrictMode. -- issue with Tooltip
	// <React.StrictMode>
	<Provider store={store}>
		<StylesProvider injectFirst>
			<MuiThemeProvider theme={theme}>
				<StyledThemeProvider theme={theme}>
					<BrowserRouter>
						<ConnectedRoot />
					</BrowserRouter>
				</StyledThemeProvider>
			</MuiThemeProvider>
		</StylesProvider>
	</Provider>,
	document.getElementById('root'),
);
