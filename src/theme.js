import { createTheme } from '@material-ui/core/styles';
import { amber, lime } from '@material-ui/core/colors';

const theme = createTheme({
	palette: {
		type: 'dark',
		primary: { main: lime[600] },
		secondary: { main: amber[600] },
	},

	overrides: {
		MuiTypography: {
			h1: { fontSize: '3rem' },
			h2: { fontSize: '2.5rem' },
			h3: { fontSize: '2rem' },
		},
	},
});

export default theme;
