import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { Button } from '@material-ui/core';

import { Alert } from '../components/mui';
import WumpusSighting from '../components/WumpusSighting';
import { gameActions } from '../redux';
import Gameplay from '../wumpus/Gameplay';

// REVIEW The game is turn-based
// REVIEW each turn begins with the player being told which cave they are in and which caves are connected to it by tunnels
// REVIEW player turn (b) player can elect to shoot one of their five "crooked arrows" (ability to change direction while in flight)
// REVIEW Each cave is connected to three others, and the system as a whole is equivalent to a dodecahedron.

// REVIEW upon moving to a new empty cave, the game describes if they can, from a pit in one of the connected caves:
//  - smell a Wumpus (wumpus)
//  - hear a bat (bat)
//  - feel a draft (pit)
//  - see a glint (gold)
//  - ¿taste? the exit (exit)
// REVIEW entering the cave with the Wumpus startles it
//  - the Wumpus will either move to another cave
//  - or remain and kill the player
//  - unlike the player, the Wumpus is not affected by super bats or pits
// REVIEW If the player chooses to fire an arrow
//  - they first select how many caves, up to five, that the arrow will travel through
//  - then enters each cave that the arrow moves through
//  - If the player enters a cave number that is not connected to where the arrow is, the game picks a valid option at random
//  - If the arrow hits the player while it is travelling, the player loses
//  - if it hits the Wumpus, they win
//  - If the arrow does not hit anything, then the Wumpus is startled and may move to a new cave
// REVIEW If the Wumpus moves to the player's location, they lose.

// IDEA two gold are randomly placed in cave, collect gold and exit cave to win
export default function HuntTheWumpusPage() {
	const { active, cave } = useSelector((state) => state.game);

	const onStartGame = useCallback(() => {
		gameActions.startGame();
	}, []);

	if (!active) {
		return (
			<Alert
				severity="info"
				message={(
					<>
						<span>Game is not running.</span>
						<Button type="submit" variant="outlined" color="primary" size="small" onClick={onStartGame}>
							New Game
						</Button>
					</>
				)}
			/>
		);
	}

	if (!cave) {
		return (<div>Loading...</div>);
	}

	const gameOver = active && !(cave.agent.alive && !cave.agent.win);

	// REVIEW this whole render, it's just temp to get off the ground
	return (
		<>
			{/* TODO remove restart button */}
			<Button type="submit" variant="outlined" color="primary" onClick={onStartGame}>
				Restart
			</Button>
			<br />

			{cave.agent.win && (<Alert severity="success" message="You win!" />)}
			{!cave.agent.alive && (<Alert severity="error" message="You lose!" />)}
			{!gameOver && (<Gameplay />)}

			<WumpusSighting cave={cave.agent.sense()} />
		</>
	);
}
