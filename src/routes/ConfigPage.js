import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { Accordion, AccordionSummary, Button, FormControlLabel, Radio, Typography } from '@material-ui/core';
import styled from 'styled-components';

import { Alert } from '../components/mui';
import { Input, Radios } from '../components/tags';
import PeasDescription from '../components/PeasDescription';
import { configActions } from '../redux';
import utils from '../utils';

const StyledContainer = styled.form`
	margin: ${(props) => props.theme.spacing(1)}px;

	.game-active {
		margin: ${(props) => props.theme.spacing(2, 0)};
	}

	.MuiCollapse-root {
		:not(.MuiCollapse-hidden) {
			padding: ${(props) => props.theme.spacing(0, 2)};
		}
		&.MuiCollapse-entered {
			padding-bottom: ${(props) => props.theme.spacing(2)}px;
		}

		div[role="region"] {
			> * + * {
				margin-top: ${(props) => props.theme.spacing(3)}px;
			}
		}
	}

	dl {
		margin: 0;
		padding: 0;

		display: grid;
		grid-template-columns: min-content auto;
		grid-template-rows: min-content;
		grid-column-gap: ${(props) => props.theme.spacing(3)}px;
		grid-row-gap: ${(props) => props.theme.spacing(1)}px;

		&.keys {
			grid-template-columns: min-content min-content auto;
		}

		dt, dd {
			margin: 0;
			padding: 0;
		}
		dt {
			white-space: nowrap;
			font-weight: bold;

			&.key {
				text-align: center;
			}
		}

		dd {
			&.forward {
				display: flex;
				flex-direction: column;
			}
			&.reverse {
				display: flex;
				flex-direction: column-reverse;
			}
		}

		[disabled] {
			color: ${(props) => props.theme.palette.text.disabled};
			font-style: italic;
		}
	}

	.form-actions {
		margin: ${(props) => props.theme.spacing(2, 0)};
	}
`;

export default function ConfigPage() {
	const { game, room } = useSelector((state) => state.config);
	const { active, cave } = useSelector((state) => state.game);
	const navigate = useNavigate();

	const [showEnvironmentConfig, setShowEnvironmentConfig] = useState(true);
	const toggleShowEnvironmentConfig = useCallback(() => { setShowEnvironmentConfig(!showEnvironmentConfig); }, [showEnvironmentConfig]);
	const [showAdvancedConfig, setShowAdvancedConfig] = useState(false);
	const toggleShowAdvancedConfig = useCallback(() => { setShowAdvancedConfig(!showAdvancedConfig); }, [showAdvancedConfig]);
	const [showKeyConfig, setShowKeyConfig] = useState(false);
	const toggleShowKeyConfig = useCallback(() => { setShowKeyConfig(!showKeyConfig); }, [showKeyConfig]);

	const handleGameChange = useCallback(([name, value]) => {
		configActions.updateConfig({
			game: {
				...game,
				[name]: value,
			},
		});
	}, [game]);

	const handleRoomChange = useCallback(([name, value]) => {
		if (name === 'roomCount') name = 'count';
		configActions.updateConfig({
			room: {
				...room,
				[name]: value,
			},
		});
	}, [room]);

	const goToGame = useCallback(() => {
		navigate('/wumpus');
	}, [navigate]);

	const onStartGame = useCallback(() => {
		configActions.startGame();
		goToGame();
	}, [goToGame]);

	const gameOver = active && !(cave.agent.alive && !cave.agent.win);

	return (
		<StyledContainer onSubmit={utils.stopEvent}>
			<Typography variant="h1">Configuration</Typography>

			<Alert
				className="game-active"
				severity="info"
				message={active && !gameOver ? (
					<>
						<span>Game is paused.</span>
						<Button type="submit" variant="outlined" color="primary" size="small" onClick={goToGame}>
							Resume
						</Button>
					</>
				) : (
					<>
						<span>Game is not running.</span>
						<Button type="submit" variant="outlined" color="primary" size="small" onClick={onStartGame}>
							New Game
						</Button>
					</>
				)}
			/>

			<Accordion expanded={showEnvironmentConfig} onChange={toggleShowEnvironmentConfig}>
				<AccordionSummary>Environment</AccordionSummary>

				<Radios
					id="game-chance"
					name="chance"
					value={game.chance}
					valueChange={handleGameChange}
					helperText="does my wheel slip because the dirt isn't solid (ice physics)"
				>
					<FormControlLabel
						value="deterministic"
						control={<Radio />}
						label={<span>Deterministic - <PeasDescription value="deterministic" /></span>}
					/>
					<FormControlLabel
						value="stochastic"
						control={<Radio />}
						label={<span>Stochastic - <PeasDescription value="stochastic" /></span>}
					/>
				</Radios>

				<Radios
					id="game-grain"
					name="grain"
					value={game.grain}
					valueChange={handleGameChange}
					helperText="digital vs analog; move in chunks vs move via velocity (w/ noop)"
				>
					<FormControlLabel value="discrete" control={<Radio />} label={<span>Discrete - <PeasDescription value="discrete" /></span>} />
					<FormControlLabel value="continuous" control={<Radio />} label={<span>Continuous - <PeasDescription value="continuous" /></span>} />
				</Radios>

				<Radios
					id="game-observable"
					name="observable"
					value={game.observable}
					valueChange={handleGameChange}
					helperText="fog of war; you remember where you were with some probability"
				>
					<FormControlLabel value="fully" control={<Radio />} label={<span>Fully - <PeasDescription value="fully" /></span>} />
					<FormControlLabel value="remember" control={<Radio />} label={<span>Remember - <PeasDescription value="remember" /></span>} />
					<FormControlLabel value="partially" control={<Radio />} label={<span>Partially - <PeasDescription value="partially" /></span>} />
				</Radios>

				<Radios
					id="game-timing"
					name="timing"
					value={game.timing}
					valueChange={handleGameChange}
					helperText="the world moves without you; or only updates when you tell it to"
				>
					<FormControlLabel value="static" control={<Radio />} label={<span>Static - <PeasDescription value="static" /></span>} />
					<FormControlLabel value="dynamic" control={<Radio />} label={<span>Dynamic - <PeasDescription value="dynamic" /></span>} />
				</Radios>

				<Radios
					id="game-apriori"
					name="apriori"
					value={game.apriori}
					valueChange={handleGameChange}
					disabled // TODO implement game.apriori
					helperText="are you alone, or do you have to battle/coop with others (is there a wumpus?)"
				>
					<FormControlLabel value="known" control={<Radio />} label={<span>Known - <PeasDescription value="known" /></span>} />
					<FormControlLabel value="unknown" control={<Radio />} label={<span>Unknown - <PeasDescription value="unknown" /></span>} />
				</Radios>

				<Radios
					id="game-agents"
					name="agents"
					value={game.agents}
					valueChange={handleGameChange}
					helperText="how much do you know about the world before you start?"
				>
					<FormControlLabel value="single" control={<Radio />} label={<span>Single - <PeasDescription value="single" /></span>} />
					<FormControlLabel value="multiple" control={<Radio />} label={<span>Multiple - <PeasDescription value="multiple" /></span>} />
				</Radios>

				<Radios
					id="game-statefulness"
					name="statefulness"
					value="sequential"
					valueChange={handleGameChange}
					disabled
					helperText="are decision related? sorting marbles vs solving a maze (a maze is sequential by definition)"
				>
					<FormControlLabel value="episodic" control={<Radio />} label={<strike>Episodic - <PeasDescription value="episodic" /></strike>} />
					<FormControlLabel value="sequential" control={<Radio />} label={<span>Sequential - <PeasDescription value="sequential" /></span>} />
				</Radios>

				{/* XXX input min/max */}
				<Input
					id="room-count"
					name="roomCount"
					type="number"
					value={room.count}
					valueChange={handleRoomChange}
					startAdornment={<span>({room.count})</span>}
				/>

				<Radios
					id="game-player"
					name="player"
					value={game.player}
					valueChange={handleGameChange}
					disabled // TODO implement game.player
					helperText="who is playing the game? a person or the lm?"
				>
					<FormControlLabel value="person" control={<Radio />} label={<span>Person - <PeasDescription value="person" /></span>} />
					<FormControlLabel value="lemon" control={<Radio />} label={<span>Lemon - <PeasDescription value="lemon" /></span>} />
				</Radios>
			</Accordion>

			{/* TODO Complications */}

			<Accordion expanded={showAdvancedConfig} onChange={toggleShowAdvancedConfig}>
				<AccordionSummary>Advanced</AccordionSummary>
				{/* TODO Advanced */}
			</Accordion>

			<Accordion expanded={showKeyConfig} onChange={toggleShowKeyConfig}>
				<AccordionSummary>Key Config</AccordionSummary>
				{/* TODO apriori - update game descriptions */}
				{/*  - we can't remove knowledge from a person, but we can show what info will be given to the agent */}

				<dl className="keys">
					<dt>Left</dt>
					<dt className="key">←</dt>
					<dd className={game.grain === 'discrete' ? 'forward' : 'reverse'}>
						<div disabled={game.grain !== 'discrete'}><em>(Discrete)</em> Turn the agent left.</div>
						<div disabled={game.grain !== 'continuous'}><em>(Continuous)</em> Apply force to increase agent count-clockwise velocity.</div>
					</dd>

					<dt>Rifght</dt>
					<dt className="key">→</dt>
					<dd className={game.grain === 'discrete' ? 'forward' : 'reverse'}>
						<div disabled={game.grain !== 'discrete'}><em>(Discrete)</em> Turn the agent right.</div>
						<div disabled={game.grain !== 'continuous'}><em>(Continuous)</em> Apply force to increase agent clockwise velocity.</div>
					</dd>

					<dt>Forward</dt>
					<dt className="key">↑</dt>
					<dd className={game.grain === 'discrete' ? 'forward' : 'reverse'}>
						<div disabled={game.grain !== 'discrete'}><em>(Discrete)</em> Move the agent forward.</div>
						<div disabled={game.grain !== 'continuous'}><em>(Continuous)</em> Apply force to increase agent velocity.</div>
					</dd>

					<dt disabled={game.grain !== 'continuous'}>Backward</dt>
					<dt disabled={game.grain !== 'continuous'} className="key">↓</dt>
					<dd disabled={game.grain !== 'continuous'}><em>(Continuous Only)</em> Apply force to decrease agent velocity.</dd>

					<dt>Grab</dt>
					<dt className="key">G</dt>
					<dd>Grab the gold. The agent must be in a room with the gold.</dd>

					<dt>Exit</dt>
					<dt className="key">E</dt>
					<dd>Exit the cave. The agent must have the gold and be in a room with an exit.</dd>

					{/* TODO update Fire description when implemented */}
					<dt disabled={game.agents !== 'multiple'}>Fire</dt>
					<dt disabled={game.agents !== 'multiple'} className="key">F</dt>
					<dd disabled={game.agents !== 'multiple'}><em>(Multiple Only)</em> Fire an arrow.</dd>

					<dt disabled={game.timing !== 'static'}>Noop</dt>
					<dt disabled={game.timing !== 'static'} className="key">&#9251;</dt>
					<dd disabled={game.timing !== 'static'}><em>(Static Only)</em> Take no action for a round.</dd>
				</dl>
			</Accordion>

			<div className="form-actions">
				<Button type="submit" variant="contained" color="primary" onClick={onStartGame}>
					New Game
				</Button>
			</div>
		</StyledContainer>
	);
}
