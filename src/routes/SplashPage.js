import React, { useEffect, useState } from 'react';
import { Typography } from '@material-ui/core';
import styled from 'styled-components';

const StyledLogo = styled.img`
	height: 40vmin;
	pointer-events: none;
	user-select: none;

	@media (prefers-reduced-motion: no-preference) {
		animation: App-logo-spin infinite 20s linear;
	}

	@keyframes App-logo-spin {
		from {
			transform: rotate(0deg);
		}
		to {
			transform: rotate(360deg);
		}
	}
`;

const StyledGrid = styled.div`
	display: grid;
	grid-template-columns: 1fr 1fr;
	grid-template-rows: min-content;
	grid-column-gap: 0.5ch;

	label {
		text-align: right;
		line-height: 1rem;
	}
	output {
		text-align: left;
		line-height: 1rem;
	}
`;

function cleanValue(value) {
	switch (value) {
	case true: return 'True';
	case false: return 'False';
	default: return value;
	}
}

export default function Splash() {
	const [message, setMessage] = useState([['loading', 'waiting...']]);

	useEffect(() => {
		let cancelled = false;
		fetch('/ping')
			.then((response) => response.json())
			.then((data) => { if (!cancelled) setMessage(Array.from(Object.entries(JSON.parse(data.message)))); });

		return () => {
			cancelled = true;
		};
	}, []);

	return (
		<header style={{ textAlign: 'center' }}>
			<StyledLogo src={`${process.env.PUBLIC_URL}/lemon-icon.png`} alt="logo" />
			<StyledGrid>
				{message.map(([label, value]) => (
					<React.Fragment key={label}>
						<Typography
							className="label"
							component="label"
							variant="body1"
							htmlFor={label}
						>
							{label}:
						</Typography>
						<Typography
							className="value"
							component="output"
							variant="body2"
							color="textSecondary"
							name={label}
						>
							{cleanValue(value)}
						</Typography>
					</React.Fragment>
				))}
			</StyledGrid>
		</header>
	);
}
