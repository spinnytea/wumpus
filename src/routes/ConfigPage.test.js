import React from 'react';
import { render } from '@testing-library/react';

import { getConfig, Test } from '../Test';
import { configActions } from '../redux';

import ConfigPage from './ConfigPage';

describe('ConfigPage', () => {
	test('renders', () => {
		configActions.updateConfig(getConfig({}));
		configActions.endGame();
		const { asFragment } = render(<Test><ConfigPage /></Test>);
		expect(asFragment()).toMatchSnapshot();
	});
});