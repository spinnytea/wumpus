import React from 'react';
import { render } from '@testing-library/react';

import { getConfig, Test } from '../Test';
import Cave from '../wumpus/Cave';

import WumpusSighting from './WumpusSighting';
import Agent from '../wumpus/Agent';

describe('WumpusSighting', () => {
	test('ddfsksp', () => {
		const config = getConfig({});
		const cave = Cave.generate(config);

		const { asFragment } = render(<Test><WumpusSighting cave={cave.agent.sense()} /></Test>);
		expect(asFragment()).toMatchSnapshot();
	});

	test.todo('Sdfsksp');

	test('dCfsksp', () => {
		const config = getConfig({ grain: 'continuous' });
		const cave = Cave.generate(config);

		const { asFragment } = render(<Test><WumpusSighting cave={cave.agent.sense()} /></Test>);
		expect(asFragment()).toMatchSnapshot();
	});

	test('ddRsksp', () => {
		const config = getConfig({ observable: 'remember' });
		const cave = Cave.generate(config);

		let { asFragment } = render(<Test><WumpusSighting cave={cave.agent.sense()} /></Test>);
		expect(asFragment()).toMatchSnapshot();

		// move forward to prove
		const next = cave.update({ forward: true });
		({ asFragment } = render(<Test><WumpusSighting cave={next.agent.sense()} /></Test>));
		expect(asFragment()).toMatchSnapshot();
	});

	test('ddPsksp', () => {
		const config = getConfig({ observable: 'partially' });
		const cave = Cave.generate(config);

		let { asFragment } = render(<Test><WumpusSighting cave={cave.agent.sense()} /></Test>);
		expect(asFragment()).toMatchSnapshot();

		// move forward to prove
		const next = cave.update({ forward: true });
		({ asFragment } = render(<Test><WumpusSighting cave={next.agent.sense()} /></Test>));
		expect(asFragment()).toMatchSnapshot();
	});

	test.todo('ddfSksp');

	test.todo('ddfsKsp');

	test('ddfskMp', () => {
		const config = getConfig({ agents: 'multiple' });
		const cave = Cave.generate(config);
		cave.arrow = new Agent({ ...cave.agent });

		const { asFragment } = render(<Test><WumpusSighting cave={cave.agent.sense()} /></Test>);
		expect(asFragment()).toMatchSnapshot();
	});

	// remember / multiple
	// TODO demonstrate that the "memory" does not match the "actual"
	test.todo('ddRskMp');

	test.todo('ddfsksL');
});