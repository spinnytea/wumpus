import React from 'react';
import PropTypes from 'prop-types';
import { amber, brown, deepOrange, green, pink, purple } from '@material-ui/core/colors';
import styled from 'styled-components';

import Agent from '../wumpus/Agent';
import Room from '../wumpus/Room';
import { useMemoizeOne } from '../utils';

const StyledSvg = styled.svg`
	border: 1px solid ${(props) => props.theme.palette.divider};
	background: ${(props) => props.theme.palette.background.paper};

	.room, .door {
		fill: none;
		stroke: ${(props) => props.theme.palette.text.secondary};
		stroke-width: 1px;
	}
	.arrow {
		fill: none;
		stroke: ${(props) => purple[props.theme.palette.type === 'dark' ? 300 : 700]};
		stroke-width: 0.8px;
	}
	.wumpus {
		fill: none;
		stroke: ${brown[300]};
		stroke-width: 1px;
	}
	.agent {
		fill: none;
		stroke: ${(props) => props.theme.palette.text.primary};
		stroke-width: 1px;
	}

	text {
		text-anchor: middle;
		alignment-baseline: middle;
	}

	text.id {
		fill: ${(props) => props.theme.palette.text.secondary};
		stroke: none;
	}
	text.exit, .agent.exit { fill: ${(props) => green[props.theme.palette.type === 'dark' ? 300 : 700]}; }
	text.gold, .agent.gold { fill: ${(props) => amber[props.theme.palette.type === 'dark' ? 300 : 700]}; }
	text.pit, .agent.pit { fill: ${(props) => deepOrange[props.theme.palette.type === 'dark' ? 300 : 700]}; }
	text.bat, .agent.bat { fill: ${(props) => pink[props.theme.palette.type === 'dark' ? 300 : 700]}; }
	text.wumpus, .agent.wumpus { fill: ${brown[300]}; }
	text.arrow, .agent.arrow, .wumpus.arrow { fill: ${(props) => purple[props.theme.palette.type === 'dark' ? 300 : 700]}; }

	.sense.exit { fill: none; stroke: ${(props) => green[props.theme.palette.type === 'dark' ? 300 : 700]}; stroke-width: 2px; }
	.sense.gold { fill: none; stroke: ${(props) => amber[props.theme.palette.type === 'dark' ? 300 : 700]}; stroke-width: 2px; }
	.sense.pit { fill: none; stroke: ${(props) => deepOrange[props.theme.palette.type === 'dark' ? 300 : 700]}; stroke-width: 2px; }
	.sense.bat { fill: none; stroke: ${(props) => pink[props.theme.palette.type === 'dark' ? 300 : 700]}; stroke-width: 2px; }
	.sense.wumpus { fill: none; stroke: ${brown[300]}; stroke-width: 2px; }
`;

function calcViewPort(config, rooms, remember) {
	const viewPort = { minx: Infinity, miny: Infinity, maxx: -Infinity, maxy: -Infinity };
	function calcBounds(room) {
		viewPort.minx = Math.min(viewPort.minx, room.x - config.room.radius * 2);
		viewPort.maxx = Math.max(viewPort.maxx, room.x + config.room.radius * 2);
		viewPort.miny = Math.min(viewPort.miny, room.y - config.room.radius * 2);
		viewPort.maxy = Math.max(viewPort.maxy, room.y + config.room.radius * 2);
	}
	rooms.forEach(calcBounds);
	if (remember?.rooms) remember.rooms.forEach(calcBounds);
	viewPort.width = viewPort.maxx - viewPort.minx;
	viewPort.height = viewPort.maxy - viewPort.miny;

	return {
		width: viewPort.width,
		height: viewPort.height,
		viewBox: `${viewPort.minx} ${viewPort.miny} ${viewPort.width} ${viewPort.height}`,
	};
}

// REVIEW Cave.js and draw _everything_
export default function WumpusSighting({ cave: { config, rooms, remember, agent, wumpus, arrow } }) {
	return (
		// eslint-disable-next-line react/jsx-props-no-spreading
		<StyledSvg {...calcViewPort(config, rooms, remember)}>
			{remember?.rooms?.map((room) => (
				<RoomG key={room.id} room={room} config={config} remember />
			))}
			{rooms.map((room) => (
				<RoomG key={room.id} room={room} config={config} />
			))}
			{!!wumpus && (<AgentG className="wumpus" agent={wumpus} config={config} />)}
			{!!agent && (<AgentG className="agent" agent={agent} config={config} />)}
			{!!arrow && (<ArrowG className="arrow" agent={arrow} config={config} />)}
		</StyledSvg>
	);
}

WumpusSighting.propTypes = {
	// cave: PropTypes.instanceOf(Cave).isRequired, // XXX agent.sense cannot return type Cave
	cave: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

// REVIEW Room.js and draw _everything_
function RoomG({ room, config, remember }) {
	const letterSize = +(config.room.radius * 0.5).toFixed(3);
	const letterOffset = +(letterSize * 0.75).toFixed(3);
	const sense = room.sense();
	const showSense = (config.game.observable === 'partially' || config.game.observable === 'remember');

	// XXX refine remember
	return (
		<g transform={`translate(${room.x.toFixed(3)},${room.y.toFixed(3)})`} opacity={remember ? '0.5' : '1'}>
			<circle className="room" r={config.room.radius} />

			<text className="id" x={0} y={-letterOffset * 2} fontSize={letterSize}>{room.id}</text>

			{room.hasExit && <text className="exit" x={-letterOffset} y={-letterOffset} fontSize={letterSize}>E</text>}
			{room.hasGold && <text className="gold" x={letterOffset} y={-letterOffset} fontSize={letterSize}>G</text>}
			{room.hasPit && <text className="pit" x={-letterOffset} y={letterOffset} fontSize={letterSize}>P</text>}
			{room.hasBat && <text className="bat" x={letterOffset} y={letterOffset} fontSize={letterSize}>B</text>}

			{/* TODO refine render sense */}
			{/* BUG sense isn't going away after we collect something - bat/gold/etc */}
			{showSense && sense.hasExit && <circle className="sense exit" r={config.room.radius + 2} />}
			{showSense && sense.hasGold && <circle className="sense gold" r={config.room.radius + 4} />}
			{showSense && sense.hasPit && <circle className="sense pit" r={config.room.radius + 6} />}
			{showSense && sense.hasBat && <circle className="sense bat" r={config.room.radius + 8} />}
			{showSense && sense.hasWumpus && <circle className="sense wumpus" r={config.room.radius + 10} />}

			{/* TODO refine render door, size */}
			{room.doors().map((d, idx) => (
				<circle key={idx} className="door" cx={d.x} cy={d.y} r="5" /> // eslint-disable-line react/no-array-index-key
			))}
		</g>
	);
}

RoomG.propTypes = {
	room: PropTypes.instanceOf(Room).isRequired,
	config: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
	remember: PropTypes.bool,
};

RoomG.defaultProps = {
	remember: false,
};

// REVIEW Agent.js and draw _everything_
function AgentG({ className, agent: { x, y, r, da, dt, alive, win, gold, hazard }, config }) {
	const agentRadius = config.agent.radius;
	const mx = Math.cos(0.4) * agentRadius;
	const my = Math.sin(0.4) * agentRadius;

	let hasHazard = hazard || '';
	if (alive) {
		if (win) hasHazard = 'exit';
		else if (gold) hasHazard = 'gold';
	}

	return (
		<g
			className={`${className} ${hasHazard}`}
			transform={`translate(${x.toFixed(3)},${y.toFixed(3)}) rotate(${(r * (180 / Math.PI)).toFixed(3)})`}
		>
			<path d={`M ${mx},${my} A ${agentRadius},${agentRadius} 0,1,1 ${mx},-${my} L 0,0 Z`} />

			{/* TODO forward speed is TOO big */}
			{/* TODO cannot see dt without da */}
			{config.game.grain === 'continuous' && (
				<line x1="0" y1="0" x2={Math.cos(dt) * da * agentRadius} y2={Math.sin(dt) * da * agentRadius} />
			)}
		</g>
	);
}

AgentG.propTypes = {
	className: PropTypes.string.isRequired,
	config: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
	agent: PropTypes.instanceOf(Agent).isRequired,
};

function makeArrowGPathD(outerRadius) {
	const innerRadius = outerRadius / 4;
	const p = (angle, radius) => `${(Math.cos(angle) * radius).toFixed(3)},${(Math.sin(angle) * radius).toFixed(3)}`;
	const g1 = 0.8;
	const g2 = 0;

	return `
		M ${outerRadius},${0} L ${p(g1, innerRadius)}

		A ${innerRadius},${innerRadius} 0,0,1 ${p(Math.PI * 0.5 - g2, innerRadius)}
		L ${p(Math.PI * 0.5, outerRadius)} L ${p(Math.PI * 0.5 + g2, innerRadius)}

		A ${innerRadius},${innerRadius} 0,0,1 ${p(Math.PI - g1, innerRadius)}
		L ${p(Math.PI, outerRadius)} L ${p(Math.PI + g1, innerRadius)}

		A ${innerRadius},${innerRadius} 0,0,1 ${p(Math.PI * 1.5 - g2, innerRadius)}
		L ${p(Math.PI * 1.5, outerRadius)} L ${p(Math.PI * 1.5 + g2, innerRadius)}

		A ${innerRadius},${innerRadius} 0,0,1 ${p(Math.PI * 2.0 - g1, innerRadius)}
		Z
	`.replace(/\s+/g, ' ');
}

function ArrowG({ className, agent: { x, y, r }, config }) {
	const d = useMemoizeOne(makeArrowGPathD, [config.agent.radius_arrow]);

	return (
		<g
			className={`${className}`}
			transform={`translate(${x.toFixed(3)},${y.toFixed(3)}) rotate(${(r * (180 / Math.PI)).toFixed(3)})`}
		>
			<path d={d} />
		</g>
	);
}

ArrowG.propTypes = {
	className: PropTypes.string.isRequired,
	config: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
	agent: PropTypes.instanceOf(Agent).isRequired,
};
