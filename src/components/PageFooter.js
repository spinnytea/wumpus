import React from 'react';
import { Paper } from '@material-ui/core';
import styled from 'styled-components';

import { Divider } from './mui';
import { Link } from './tags';

const StyledFooter = styled(Paper)`
	margin-top: ${(props) => props.theme.spacing(2)}px;
	padding: ${(props) => props.theme.spacing(2, 1)};

	display: flex;
	align-items: center;
	justify-content: center;
	gap: ${(props) => props.theme.spacing(1)}px;

	a {
		color: ${(props) => props.theme.palette.text.secondary};
	}
`;

export default function PageFooter() {
	return (
		<StyledFooter component="footer" elevation={0} square>
			<Link to="/wumpus">Hunt the Wumpus</Link>
			<Divider orientation="vertical" />
			<Link to="/config">Config</Link>
			<Divider orientation="vertical" />
			<Link to="/splash">Splash</Link>
		</StyledFooter>
	);
}
