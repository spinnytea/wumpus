import React from 'react';
import PropTypes from 'prop-types';
import { Alert as MuiAlert } from '@material-ui/lab';
import styled from 'styled-components';

const StyledAlert = styled(MuiAlert)`
	display: flex;
	align-items: center;
	justify-content: flex-start;

	.MuiAlert-icon {
		margin-right: ${(props) => props.theme.spacing(1.5)}px;
	}

	.MuiAlert-message {
		display: flex;
		align-items: center;
		justify-content: flex-start;
		gap: ${(props) => props.theme.spacing(1.5)}px;
	}
`;

export default function Alert({ className, message, severity }) {
	if (!message) return null;

	return (
		<StyledAlert className={className} severity={severity}>
			{message}
		</StyledAlert>
	);
}

Alert.propTypes = {
	className: PropTypes.string,
	message: PropTypes.node,
	severity: PropTypes.oneOf(['success', 'info', 'warning', 'error']),
};

Alert.defaultProps = {
	className: undefined,
	message: undefined,
	severity: 'error',
};
