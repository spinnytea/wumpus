import React from 'react';
import PropTypes from 'prop-types';

function getMessage(value) {
	switch (value) {
	// chance
	case 'deterministic': return 'Actions are guaranteed. Everything happens exactly as described.';
	case 'stochastic': return 'Actions have upredictable outcomes. Sometimes the movement will slip or an action will fail.';
	// grain
	case 'discrete': return 'The world is in exactly countable blocks. Like counting apples or a "block world".';
	case 'continuous': return 'The world is analog and has ranges. Like measuring a table or throwing darts.';
	// observable
	case 'fully': return 'Every part of the game can be scene. All information is visible.';
	case 'remember': return 'Fog of war. Only the immediate surroundings can be sensed, '
		+ 'but previous states will be automatically remembered as observable.';
	case 'partially': return 'Only the immediate surroundings can be sensed. Some information must be intuited or sought out.';
	// timing
	case 'static': return 'The world is fixed. It does not change until/unless you take action.';
	case 'dynamic': return 'The world moves forward with or without you.';
	// apriori
	// TODO game.apriori should be broken down into multiple levels
	//  1. know everything (your actions, win state, fail state, other rules of the world (peas))
	//  2. know game outline (your actions, win state, fail state)
	//  3. know objective (your actions, win state)
	//  4. know just you (your actions)
	//  5. know the goal (win state)
	//  6. know nothing ()
	case 'known': return 'Before you start, you know all the rules: Actions you can take, how to win, how to lose, all set peices in the game.';
	case 'unknown': return 'You do not know the rules the world.';
	// agents
	// XXX game.agents can also be broken up
	//  1. agent
	//  2. coop
	//  3. agent + wumpus
	//  4. coop + wumpus
	//  5. agent + wumpi
	//  6. coop + wumpi
	case 'single': return 'You are alone in the game. There are other peices that can act, but they follow simple rules.';
	case 'multiple': return 'Theory of mind. You are with other agents in the world. They may work with you or work against you.';
	// statefulness
	case 'episodic': return 'Each "epoch" is fully independent. Sorting marbles does not require tracking state. Assess, act, assess, act.';
	case 'sequential': return 'Decisions impact the future, you can learn from the past. Solving a maze helps to know when / when not to retrace steps.';
	// player
	case 'lemon': return 'LM player. The entire point of this whole enterprise.';
	case 'person': return 'Human player, keyboard input. For fun, profit, and testing.';

	default: return '';
	}
}

export default function PeasDescription({ value }) {
	const message = getMessage(value);

	if (message) {
		return (
			<span>{message}</span>
		);
	}

	return null;
}

PeasDescription.propTypes = {
	value: PropTypes.string.isRequired,
};
