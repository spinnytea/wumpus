import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';
import { Link as MuiLink } from '@material-ui/core';

export default function Link({ children, className, to }) {
	return (
		<MuiLink to={to} className={className} color="primary" component={RouterLink}>
			{children}
		</MuiLink>
	);
}

Link.propTypes = {
	// required / the link
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	to: PropTypes.string.isRequired,

	// optional
	className: PropTypes.string,
};

Link.defaultProps = {
	className: undefined,
};
