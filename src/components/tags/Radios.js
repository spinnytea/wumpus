import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormControl, FormHelperText, FormLabel, RadioGroup } from '@material-ui/core';
import utils from '../../utils';

export default function Radios(props) {
	const {
		children: options,
		className,
		disabled,
		display = utils.nameToDisplay(props.name), // eslint-disable-line react/destructuring-assignment
		helperText,
		id = props.name, // eslint-disable-line react/destructuring-assignment
		name,
		required,
		value,
		valueChange,
	} = props;

	const onChange = useCallback((event) => {
		if (valueChange) {
			valueChange([name, event.target.value, event]);
		}
	}, [valueChange, name]);

	return (
		<FormControl
			className={className}
			disabled={disabled}
			fullWidth
			required={required}
			component="fieldset"
		>
			<FormLabel component="legend">{display}</FormLabel>
			{!!helperText && (<FormHelperText>{helperText}</FormHelperText>)}
			<RadioGroup
				value={value}
				onChange={onChange}
				name={name}
				id={id}
			>
				{options}
			</RadioGroup>
		</FormControl>
	);
}

Radios.propTypes = {
	// i/o
	value: PropTypes.any, // eslint-disable-line react/forbid-prop-types
	valueChange: PropTypes.func,
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,

	// optional
	className: PropTypes.string,
	disabled: PropTypes.bool,
	display: PropTypes.string,
	helperText: PropTypes.string,
	id: PropTypes.string,
	name: PropTypes.string,
	required: PropTypes.bool,
};

Radios.defaultProps = {
	className: undefined,
	disabled: false,
	display: undefined,
	helperText: undefined,
	id: undefined,
	name: undefined,
	required: false,
	value: '',
	valueChange: null,
};
