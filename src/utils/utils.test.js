import utils from '.';

describe('utils', () => {
	test.todo('nameToDisplay');

	test('random', () => {
		utils.random.seed('hello.');
		expect(utils.random.next()).toBe(0.9282578795792454);
		expect(utils.random.next()).toBe(0.3752569768646784);

		utils.random.seed('world.');
		expect(utils.random.next()).toBe(0.6539801709244648);
		expect(utils.random.next()).toBe(0.26727245610092054);

		utils.random.seed('hello.');
		expect(utils.random.next()).toBe(0.9282578795792454);
		expect(utils.random.next()).toBe(0.3752569768646784);
	});

	describe('time', () => {
		test.todo('format');

		test.todo('formatNow');
	});

	test.todo('stopEvent');
});