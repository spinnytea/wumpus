import { useState } from 'react';
import memoizeOne from 'memoize-one';

export default function useMemoizeOne(resultFn, args, isEqual = undefined) {
	const [memoized] = useState(() => memoizeOne(resultFn, isEqual));
	// resultFn and isEqual should be static, we should not need to watch for them to change to remake the memo
	// useEffect(() => { setMemoized(memoizeOne(resultFn, isEqual)); }, [resultFn, isEqual, setMemoized]);
	return memoized(...args);
}
