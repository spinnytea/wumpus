import { startCase } from 'lodash';
import moment from 'moment';
import seedrandom from 'seedrandom';

let prng = seedrandom();

const utils = {
	SHORT_DELAY: 400,
	LONG_DELAY: 1000,

	nameToDisplay: (name) => startCase(name),

	random: {
		seed: (seed) => { prng = seedrandom(seed); },
		next: () => prng(),
		range: (len) => Math.floor(utils.random.next() * len),
		pullItem: (list) => list.splice(utils.random.range(list.length), 1)[0],
	},

	time: {
		format: (date) => moment(date).format('MMMM Do YYYY'),
		fromNow: (date) => moment(date).fromNow(),
	},

	stopEvent: (event) => {
		event.stopPropagation();
		event.preventDefault();
	},
};

export default utils;
export { default as useMemoizeOne } from './useMemoizeOne';
export { default as useQueryParams } from './useQueryParams';
