import React from 'react';
import PropTypes from 'prop-types';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { StylesProvider, ThemeProvider as MuiThemeProvider } from '@material-ui/core';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import { cloneDeep } from 'lodash';

import { store } from './redux';
import theme from './theme';

export function Test({ children, route }) {
	return (
		<Provider store={store}>
			<StylesProvider injectFirst>
				<MuiThemeProvider theme={theme}>
					<StyledThemeProvider theme={theme}>
						<MemoryRouter route={route}>
							{children}
						</MemoryRouter>
					</StyledThemeProvider>
				</MuiThemeProvider>
			</StylesProvider>
		</Provider>
	);
}

Test.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]).isRequired,
	route: PropTypes.string,
};

Test.defaultProps = {
	route: null,
};

export function getConfig({
	// these are the 'simple' values used as defaults
	chance = 'deterministic',
	grain = 'discrete',
	observable = 'fully',
	timing = 'static',
	apriori = 'known',
	agents = 'single',
	player = 'person',
}) {
	const config = cloneDeep(store.getState().config);

	config.room.count = 5; // we only need a few for testing, use the min value
	config.room.radius = 24; // this is a nice divisible number, for test output
	config.room.spacing = 1.5;

	// apply the settings
	config.game.chance = chance;
	config.game.grain = grain;
	config.game.observable = observable;
	config.game.timing = timing;
	config.game.apriori = apriori;
	config.game.agents = agents;
	config.game.player = player;

	return config;
}
